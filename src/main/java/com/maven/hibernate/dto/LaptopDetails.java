package com.maven.hibernate.dto;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "laptop_details")
public class LaptopDetails implements Serializable{
	
	@Id
	@GenericGenerator(name = "laptop_detail_auto", strategy = "increment")
	@GeneratedValue(generator = "laptop_detail_auto")
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "BRAND")
	private String brand;
	
	@Column(name = "MODEL_NAME")
	private String modelName;
	
	@Column(name = "SPECIFICATIONS")
	private String specifications;
	
	@Column(name = "PRICE")
	private Double price;
	
	@Column(name = "USER_RATINGS")
	private Double userRatings;

	public LaptopDetails() {
		
	}

	//parameterized constructor
	public LaptopDetails(String brand, String modelName, String specifications, Double price, Double userRatings) {
		super();
		this.brand = brand;
		this.modelName = modelName;
		this.specifications = specifications;
		this.price = price;
		this.userRatings = userRatings;
	}

	//getter and setter methods
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}
	
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getUserRatings() {
		return userRatings;
	}

	public void setUserRatings(Double userRatings) {
		this.userRatings = userRatings;
	}

	
	//toString Method
	@Override
	public String toString() {
		return "LaptopDetails [id=" + id + ", brand=" + brand + ", modelName=" + modelName + ", specifications="
				+ specifications + ", price=" + price + ", userRatings=" + userRatings + "]";
	}

}
