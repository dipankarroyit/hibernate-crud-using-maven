package com.maven.hibernate.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.maven.hibernate.dto.LaptopDetails;
import com.maven.hibernate.utils.HibernateUtil;

public class LaptopDAO {
	
	public void saveLaptopDetails(LaptopDetails laptopDetails) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			
			// operation 1
			Object object = session.save(laptopDetails);
			
			// operation 2
			session.get(LaptopDetails.class, (Serializable) object);
			
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}
	
	public void insertLaptopDetails() {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();

			String hql = "INSERT INTO LaptopDetails (brand, modelName, specifications, price, userRatings) "
					+ "FROM LaptopDetails";
			Query query = session.createQuery(hql);
			int result = query.executeUpdate();
			System.out.println("Rows affected: " + result);

			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public void updateLaptopDetails(LaptopDetails laptopDetails) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();

			// save the LaptopDetails object
			String hql = "UPDATE LaptopDetails set modelName = :modelName " + "WHERE id = :modelId";
			Query query = session.createQuery(hql);
			query.setParameter("modelName", laptopDetails.getModelName());
			query.setParameter("modelId", 1);
			int result = query.executeUpdate();
			System.out.println("Rows affected: " + result);

			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public void deleteLaptopDetails(int id) {

		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();

			// Delete a LaptopDetails object
			LaptopDetails laptopDetails = session.get(LaptopDetails.class, id);
			if (laptopDetails != null) {
				String hql = "DELETE FROM LaptopDetails " + "WHERE id = :modelId";
				Query query = session.createQuery(hql);
				query.setParameter("modelId", id);
				int result = query.executeUpdate();
				System.out.println("Rows affected: " + result);
			}

			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}

	public LaptopDetails getLaptopDetails(int id) {

		Transaction transaction = null;
		LaptopDetails laptopDetails = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();

			// get an LaptopDetails object
			String hql = " FROM LaptopDetails D WHERE D.id = :modelId";
			Query query = session.createQuery(hql);
			query.setParameter("modelId", id);
			List results = query.getResultList();
			
			if (results != null && !results.isEmpty()) {
				laptopDetails = (LaptopDetails) results.get(0);
			}
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return laptopDetails;
	}

	public List<LaptopDetails> getLaptopDetails() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from LaptopDetails", LaptopDetails.class).list();
		}
	}
}