package com.maven.hibernate;

import java.util.List;
import com.maven.hibernate.dao.LaptopDAO;
import com.maven.hibernate.dto.LaptopDetails;

public class App {
	public static void main(String[] args) {
		LaptopDAO laptopDAO = new LaptopDAO();
		LaptopDetails laptopDetails = new LaptopDetails("Apple", "Macbook Air", "13.3inch, 8GB Ram, MacOS Catalina",80000D , 4.3D);
		laptopDAO.saveLaptopDetails(laptopDetails);
		
		laptopDAO.insertLaptopDetails();
		
		// update LaptopDetails
		LaptopDetails laptopDetails1 = new LaptopDetails("Lenovo", "ThinkPad", "14inch, 8GB Ram, Windows 10",60000D , 4.1D);
		laptopDAO.updateLaptopDetails(laptopDetails1);
			
		// get LaptopDetails
		List<LaptopDetails> laptopDetails2 = laptopDAO.getLaptopDetails();
		laptopDetails2.forEach(s -> System.out.println(s.getModelName()));
		
		// get single LaptopDetails
		LaptopDetails laptopDetails3 = laptopDAO.getLaptopDetails(1);
		System.out.println(laptopDetails3.getModelName());
		
		// delete LaptopDetails
		laptopDAO.deleteLaptopDetails(1); 
	}
}